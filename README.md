# OpenProject Alpine
OpenProject is a web-based project management software. Its key features are:

* [Project planning and scheduling](https://www.openproject.org/collaboration-software-features/#project-planning)
* [Product roadmap and release planning](https://www.openproject.org/collaboration-software-features/#product-management)
* [Task management and team collaboration](https://www.openproject.org/collaboration-software-features/#task-management)
* [Agile and Scrum](https://www.openproject.org/collaboration-software-features/#agile-scrum)
* [Time tracking, cost reporting and budgeting](https://www.openproject.org/collaboration-software-features/#time-tracking)
* [Bug tracking](https://www.openproject.org/collaboration-software-features/#bug-tracking)
* [Wikis](https://www.openproject.org/help/wiki/)
* [Forums](https://www.openproject.org/help/user-guides/forum/)
* [Meeting agendas and meeting minutes](https://www.openproject.org/help/meetings/)

More information and screenshots can be found on our [website](https://www.openproject.org).

## Development
If you want to build the docker image and test it for development, just type the following command:

```
docker run --name db -p 5432:5432 -e POSTGRES_USER=openproject -e POSTGRES_PASSWORD=openproject -e POSTGRES_DB=openproject_development -e DB_HOST=0.0.0.0 postgres:10-alpine
docker run --link db -p 3000:3000 -d openproject-alpine:development

```

If you want to build the complete system and run it on production, just type the following command:

```
docker-compose -f docker-compose-development.yml up -d --build

```

## Staging
If you want to build the docker image and test it for development, just type the following command:

```
docker run --name db -p 5432:5432 -e POSTGRES_USER=openproject -e POSTGRES_PASSWORD=openproject -e POSTGRES_DB=openproject_staging -e DB_HOST=0.0.0.0 postgres:10-alpine
docker run --link db -p 3000:3000 -d openproject-alpine:staging

```

If you want to build the complete system and run it on production, just type the following command:

```
docker-compose -f docker-compose-staging.yml up -d --build

```

## Production

You can test your production image executing the following commands:

```
docker run --name db -p 5432:5432 -e POSTGRES_USER=openproject -e POSTGRES_PASSWORD=openproject -e POSTGRES_DB=openproject -e DB_HOST=0.0.0.0 postgres:10-alpine
docker run --link db -p 3000:3000 -d openproject-alpine:production

```

If you want to build the complete system and run it on production, just type the following command:

```
docker-compose -f docker-compose-production.yml up -d

```

## License and Support
### Apache 2.0 License
[![License](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg)](https://opensource.org/licenses/Apache-2.0)  

This code is released into the public domain by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) under [![License](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg)](https://opensource.org/licenses/Apache-2.0)  

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) for further details.
