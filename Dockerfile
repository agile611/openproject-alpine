FROM alpine:3.6
MAINTAINER Guillem <guillem@itnove.com>
ENV RAILS_ENV production
RUN alias cp='cp -iv'
RUN alias mv='mv -iv'
RUN alias rm='rm -iv'
RUN alias less='less -MNE~'
RUN alias more=less
RUN alias ll='ls -lFah --color=auto'
RUN alias mroe=more
RUN alias moer=more
RUN alias mreo=more
RUN alias ..='cd ../'
RUN alias ...='cd ../../'
RUN alias mkdir='mkdir -pv'
RUN alias grep='grep -in'
RUN set -o vi
WORKDIR /app
RUN set -e
RUN apk --no-cache add git libffi libpq libxml2 libxslt mariadb postgresql ruby ruby-bigdecimal ruby-bundler ruby-io-console ruby-irb ruby-rdoc
RUN apk --no-cache add --virtual build_deps build-base libffi-dev libxml2-dev libxslt-dev linux-headers mariadb-dev postgresql-dev ruby-dev sqlite-dev
RUN apk --no-cache add nodejs nodejs-npm && npm install npm@latest -g
RUN git clone https://github.com/opf/openproject.git /app/openproject
COPY vendored-plugins /app/openproject/vendored-plugins
COPY setup_database /app/openproject/bin
RUN addgroup -S app
RUN adduser -S -g app app
RUN gem install bundler --version "1.15"
COPY entrypoint.sh /app/openproject
CMD chmod +x /app/openproject/entrypoint.sh
RUN chown -R app:app /app /usr/lib/ruby/gems/2.4.0 /usr/bin /app/openproject
USER app
WORKDIR /app/openproject
RUN gem install bundler
RUN bundle config build.nokogiri --use-system-libraries
RUN bundle install --deployment --without development test
RUN sed -i 's/bower install/bower install --allow-root/g' frontend/package.json
RUN npm install
COPY database.yml.postgres config/database.yml
RUN gem install foreman --no-ri --no-rdoc
RUN sed -i 's/Rails.groups(:opf_plugins)/Rails.groups(:opf_plugins, :docker)/g' config/application.rb
VOLUME ["/app/files"]
EXPOSE 3000 5432
ENTRYPOINT ["/app/openproject/entrypoint.sh"]

