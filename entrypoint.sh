#!/bin/sh
set -e
cd /app/openproject
bin/setup_database
bundle exec rake db:migrate
bundle exec rake assets:precompile
HOST=0.0.0.0 PORT=3000 foreman start web